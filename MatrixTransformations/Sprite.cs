﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixTransformations
{
    public class Sprite
    {
        private Color color;
        private Vector2 velocity;
        private float angle;
        private float rotationSpeed;
        private Vector2 scale;
        private Vector2 position;
        private Rectangle rectangle;
        private Texture2D texture;
        private Rectangle? bounds;
        private Vector2 origin;
        private Matrix transform;
        private float scaleValue;

        public Sprite(Vector2 pos, float speed = 0, float angle = 0,float rotationSpeed = 0, float scale = 1.0f, Rectangle? bounds = null)
        {
           
            this.position = pos;
            this.angle = angle;
            this.rotationSpeed = rotationSpeed;
            this.scaleValue = scale;
            this.scale = new Vector2(scale,scale);

            this.velocity = new Vector2((float)(speed * Math.Cos(angle)), (float)(speed * Math.Sin(angle)));

            this.texture = null;
            this.color = Color.White;
            this.origin = Vector2.Zero;

            this.bounds = bounds;
        }

        protected Texture2D Texture => texture;

        public Vector2 Position => position;

        public Rectangle Rectange => rectangle;

        public Vector2 Origin => origin;

        public bool Collided { get; private set; }

        public void loadContent(ContentManager content, GraphicsDevice graphicDevice, string assetName)
        {
            texture = content.Load<Texture2D>(assetName);
            OnContentLoaded(content, graphicDevice);
        }

        //Method to do something once Content is loaded.
        protected virtual void OnContentLoaded(ContentManager content, GraphicsDevice graphicsDevice)
        {
            origin = new Vector2(texture.Width / 2f, texture.Height / 2f);
            UpdateTransformMatrix();
            UpdateRectangle();
            
        }

        private void UpdateTransformMatrix()
        {
            //SRT reverse origin * scale * roatation * translation
            transform = Matrix.CreateTranslation(new Vector3(-origin, 0)) *
                Matrix.CreateScale(scaleValue) *
                Matrix.CreateRotationZ(angle) *
                Matrix.CreateTranslation(new Vector3(position, 0));

        }

        private void UpdateRectangle()
        {
            Vector2 topLeft = Vector2.Transform(Vector2.Zero, transform);
            Vector2 topRight = Vector2.Transform(new Vector2(texture.Width, 0), transform);
            Vector2 bottomLeft = Vector2.Transform(new Vector2(0, texture.Height), transform);
            Vector2 bottomRight = Vector2.Transform(new Vector2(texture.Width, texture.Height), transform);

            Vector2 min = new Vector2(MathEx.Min(topLeft.X, topRight.X, bottomLeft.X, bottomRight.X),
                MathEx.Min(topLeft.Y,topRight.Y,bottomLeft.Y,bottomRight.Y));

            Vector2 max = new Vector2(MathEx.Max(topLeft.X, topRight.X, bottomLeft.X, bottomRight.X),
                MathEx.Max(topLeft.Y, topRight.Y, bottomLeft.Y, bottomRight.Y));

            rectangle = new Rectangle((int)min.X, (int)min.Y, (int)(max.X - min.X), (int)(max.Y - min.Y));

        }

        public virtual void Unload()
        {
            texture.Dispose();
        }

        public void Update(GameTime gameTime)
        {
            position += velocity * (float)gameTime.ElapsedGameTime.TotalSeconds;

            UpdateRotation(gameTime);
            UpdateTransformMatrix();
            UpdateRectangle();
            CheckBounds();
        }

        private void UpdateRotation(GameTime gameTime)
        {
            angle += (float)(rotationSpeed * gameTime.ElapsedGameTime.TotalSeconds);

            if(angle < 0)
            {
                angle = MathHelper.TwoPi - Math.Abs(angle);
            }

            else if(angle > MathHelper.TwoPi)
            {
                angle = angle - MathHelper.TwoPi;
            }
        }

        private void CheckBounds()
        {
            if (bounds == null) return;

            Vector2 change = Vector2.Zero;
            if (rectangle.Left <= bounds.Value.X)
            {
                change.X = bounds.Value.X - rectangle.Left;
            }

            else if (rectangle.Right >= bounds.Value.Right)
            {
                change.X = bounds.Value.Right - rectangle.Right;
            }

            if (rectangle.Top <= bounds.Value.Y)
            {
                change.Y = bounds.Value.Y - rectangle.Top;
            }

            else if (rectangle.Bottom >= bounds.Value.Bottom)
            {
                change.Y = bounds.Value.Bottom - rectangle.Bottom;
            }

            if (change == Vector2.Zero) return;

            position = new Vector2((int)position.X + change.X, (int)position.Y + change.Y);
            UpdateRectangle();

        }

        public bool Collision(Sprite target)
        {
            bool intersects = rectangle.Intersects(target.rectangle) && PerPixelCollision(target);
            Collided = intersects;
            target.Collided = intersects;
            return intersects;

        }

        private bool PerPixelCollision(Sprite target)
        {

            //relativeToB * TransformB = relativeToA * TransformA.
            //RelativeB = relativeToA * TranformA *  Invert(TransformB).
            //atoB = transformA * invret(TransformB
            //relativeToB = relativeToA * AtoB

            Matrix atob = transform * Matrix.Invert(target.transform);

            var sourceColors = new Color[texture.Width * texture.Height];
            texture.GetData(sourceColors);

            var targetColors = new Color[target.texture.Width * target.texture.Height];
            target.texture.GetData(targetColors);

            Vector2 stepX = Vector2.TransformNormal(Vector2.UnitX, atob);
            Vector2 stepY = Vector2.TransformNormal(Vector2.UnitY, atob);

            Vector2 targetPosition = Vector2.Transform(Vector2.Zero,atob);

            for (int x = 0; x < texture.Width; x++)
            {
                Vector2 currentTargetPosition = targetPosition;

                for (int y = 0; y < texture.Height; y++)
                {
                    int targetX = (int)currentTargetPosition.X;
                    int targetY = (int)currentTargetPosition.Y;

                    if (targetX >= 0 && targetX < target.texture.Width && targetY >= 0 && targetY < target.texture.Height)
                    {
                        Color colorSource = sourceColors[x + y * texture.Width];
                        Color colorTarget = targetColors[targetX + targetY * target.texture.Width];

                        if (colorSource.A != 0 && colorTarget.A != 0)
                        {
                            return true;
                        }
                    }
                    currentTargetPosition += stepY;
                }

                targetPosition += stepX;
            }

            return false;
        }

        //Assume Begin was already called when using this mehtod
        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Draw(texture, position, null, null, origin, angle, scale, color);
        }
    }
}
